/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mavenproject1;

import java.util.Random;

/**
 *
 * @author student
 */
public class Main {
    
    public static void main(String[] args) {
        final int N = 10;
        int []tab = new int[N];
        
        
        for(int i = 0; i < N; i++){
            tab[i] = new Random().nextInt();
        }
        
        int min = tab[0];
        
        for(int i = 0; i < N; i++){
            if(tab[i] < min)
                min = tab[i];
        }
        
        
       for(int i = 0; i < N; i++){
           for(int j = 0; j < N-1; j++){
               if(tab[j] > tab[j+1]){
                   int pom = tab[j];
                   tab[j] = tab[j+1];
                   tab[j+1] = pom;
               }
           }
       }
        
    }
}
